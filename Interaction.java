import java.util.Scanner;

public class TestScanner {

    public static void main(String[] args) {

        try ( Scanner scanner = new Scanner( System.in ) ) {
            
            while( true ) {
                System.out.print( "Votre Choix :" );
                String choix = scanner.nextLine();
                
                if ( choix.equals( "Balle" ) ) {
                    break;
                }
            }
            
            System.out.println( "Vous êtes restauré en activité" );
            int count = scanner.nextInt();
            if ( count > 100 ) { 
                System.out.println( "Tu commence à t'amuser" );
            }
        }
        
    }
    
}